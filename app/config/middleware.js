/**
 * Middleware config
 * @return
 */
module.exports = { 
    list: ['view'], //加载的中间件列表
    config: { //中间件配置 
        static: {
            cache: false,
            preload: false
        },
        view: {
            view_path: process.env.ROOT_PATH + '/static/views/', //模板目录
            default_theme: '', //默认模板主题
        }
    }
};