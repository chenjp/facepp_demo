/**
 * Controller
 * @return
 */
const {controller, helper} = require('thinkkoa');
const rp = require('request-promise');
const jsSHA = require('jssha');

//const appId = 'wxf54acd4c1f25209c'; //MmoonBird
//const appSecret = '969c3e40058796f391fbec2639382675'; //MmoonBird
const appId = 'wx06a726555d597a4b';
module.exports = class extends controller {
    //构造方法init代替constructor
    // init(ctx, app){
    //     // property
    // }
    //所有该控制器(含子类)方法前置方法
    __before(){
        console.log('__before');
    }
    //URI定位到该控制器,如果该控制器不存在某个方法时自动调用
    __empty(){
        return this.write('can\'t find action');
    }
    
    //控制器默认方法
    // indexAction () {
    //     // return this.write('Hello, ThinkKoa!!');
    //     return this.render();
        
    // }

    /**
     * 获取微信配置
     */
    async getWxConfigAction() {
        let currentUrl = this.param('url');
        let options = {
            uri: 'http://xiaohuashijie.medsagacityidea.com/get-appDevInfo',
            json: true // Automatically parses the JSON string in the response
        };

        let accessTokenDic = await rp(options);

        let noncestr = Math.random().toString(36).substr(2, 15);
        let timestamp = parseInt(new Date().getTime() / 1000 + '') + '';

        let paramsDic = {
            jsapi_ticket: accessTokenDic.jsApiTicket,
            nonceStr: noncestr,
            timestamp: timestamp,
            url: currentUrl
        };
        let paramsStr = this.raw(paramsDic);
        let shaObj = new jsSHA(paramsStr, 'TEXT');
        paramsDic.signature = shaObj.getHash('SHA-1', 'HEX');
        delete paramsDic.jsapi_ticket;
        paramsDic.appId = appId;
        return this.json({ status: 200, message: '成功', data: paramsDic });
    }

    async _getAccessToken(){

    }

    
    raw(args) {
        let keys = Object.keys(args);
        keys = keys.sort();
        let newArgs = {};
        keys.forEach(function (key) {
            newArgs[key.toLowerCase()] = args[key];
        });

        let string = '';
        for (let k in newArgs) {
            string += '&' + k + '=' + newArgs[k];
        }
        string = string.substr(1);
        return string;
    }
};